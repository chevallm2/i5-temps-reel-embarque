#include <stdio.h>

#include "esp_system.h"
#include "driver/gpio.h"
#include "config.h"

void allumer(int nLed){
    printf("Allumer led \n");
    gpio_set_level(GPIO_NUM_13, 1) ;
}

void eteindre(int nLed){
    printf("Eteindre led \n");
    gpio_set_level(nLed, 0) ;
}
