#include <stdio.h>

#include "esp_system.h"
#include "driver/gpio.h"
#include "led.h"


void onButtonPush(){

    eteindre(13);

    //printf("Changement état bouton");

}

void config(){
    
    gpio_install_isr_service(0) ;

    // PIN OUTPUT 
    printf("Configuration pin D2 \n");
    gpio_config_t pin_2;
    pin_2.mode = GPIO_MODE_OUTPUT ;
    pin_2.pin_bit_mask = GPIO_Pin_4;
    pin_2.intr_type = GPIO_INTR_DISABLE;
    pin_2.pull_down_en = 1;
    pin_2.pull_up_en = 0;
    gpio_config(&pin_2) ;


    // printf("Configuration pin D3");
    // gpio_config_t pin_3;
    // pin_3.mode = GPIO_MODE_OUTPUT ;
    // pin_3.pin_bit_mask = GPIO_Pin_0;
    // gpio_config(&pin_3) ;

    // printf("Configuration pin D4");
    // gpio_config_t pin_4;
    // pin_4.mode = GPIO_MODE_OUTPUT ;
    // pin_4.pin_bit_mask = GPIO_Pin_2;
    // gpio_config(&pin_4) ;

    // printf("Configuration pin D5");
    // gpio_config_t pin_5;
    // pin_5.mode = GPIO_MODE_OUTPUT ;
    // pin_5.pin_bit_mask = GPIO_Pin_14;
    // gpio_config(&pin_5) ;

    // printf("Configuration pin D6");
    // gpio_config_t pin_6;
    // pin_6.mode = GPIO_MODE_OUTPUT ;
    // pin_6.pin_bit_mask = GPIO_Pin_12;
    // gpio_config(&pin_6);

    printf("Configuration pin D7 \n");
    gpio_config_t led;
    led.mode = GPIO_MODE_OUTPUT ;
    led.pin_bit_mask = GPIO_Pin_13;
    led.intr_type = GPIO_INTR_DISABLE;
    led.pull_down_en = led.pull_up_en = 0;
    gpio_config(&led) ;


    // PIN INPUT     
    printf("Configuration pin D1 \n");
    gpio_config_t pin_1;
    pin_1.mode = GPIO_MODE_INPUT ;
    pin_1.pin_bit_mask = GPIO_Pin_5;
    pin_1.intr_type = GPIO_INTR_NEGEDGE;
    pin_1.pull_down_en = 0;
    pin_1.pull_up_en = 0;
    gpio_config(&pin_1) ;
    gpio_isr_handler_add(GPIO_NUM_5, onButtonPush, NULL);


}

